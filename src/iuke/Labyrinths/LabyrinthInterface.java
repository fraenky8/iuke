package iuke.Labyrinths;

public interface LabyrinthInterface {

	/**
	 * @return
	 */
	public int getTileStartPosition();
	
	/**
	 * @return
	 */
	public int getStartPositionX();
	
	/**
	 * @return
	 */
	public int getStartPositionY();
	
	/**
	 * @return
	 */
	public int[] getStartPositionXY();
	
	/**
	 * @return
	 */
	public int getTileGoalPosition();
	
	/**
	 * @return
	 */
	public int getGoalPositionX();
	
	/**
	 * @return
	 */
	public int getGoalPositionY();
	
	/**
	 * @return
	 */
	public int[] getGoalPositionXY();
	
	/**
	 * @param x
	 * @param y
	 * @return
	 */
	public int getPdfPageForXYPosition(int x, int y);
}
