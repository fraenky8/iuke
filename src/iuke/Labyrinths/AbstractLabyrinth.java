package iuke.Labyrinths;

public abstract class AbstractLabyrinth implements LabyrinthInterface {

	protected int startTilePosition;
	protected int startX;
	protected int startY;

	protected int goalTilePosition;
	protected int goalX;
	protected int goalY;
	
	protected int[][] labyrinth;
	
	public AbstractLabyrinth() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public int getTileStartPosition() {
		return startTilePosition;
	}

	@Override
	public int getStartPositionX() {
		return startX;
	}

	@Override
	public int getStartPositionY() {
		return startY;
	}

	@Override
	public int[] getStartPositionXY() {
		int[] xy = {startX, startY}; 
		return xy;
	}

	@Override
	public int getTileGoalPosition() {
		return goalTilePosition;
	}

	@Override
	public int getGoalPositionX() {
		return goalX;
	}

	@Override
	public int getGoalPositionY() {
		return goalY;
	}

	@Override
	public int[] getGoalPositionXY() {
		int[] xy = {goalX, goalY}; 
		return xy;
	}

	public int getPdfPageForXYPosition(int x, int y) {
		// Achtung aufpassen! y und x vertauscht, da im array nach unten die y-Achse und zur Seite die x-Achse ist...
		return labyrinth[y][x];
	}
	
	
}
