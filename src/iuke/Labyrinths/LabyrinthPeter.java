package iuke.Labyrinths;

public class LabyrinthPeter extends AbstractLabyrinth implements
		LabyrinthInterface {

	public LabyrinthPeter() {
		
		// hardcoded
		
		labyrinth = new int[][] { 
				{ 12, 14, 2, 6, 6, 6, 4, 10, 6, 6, 4 },
				{ 13, 4, 11, 10, 6, 6, 7, 11, 10, 6, 7 },
				{ 12, 9, 7, 13, 6, 2, 6, 3, 13, 2, 4 },
				{ 11, 11, -1, -1, -1, 11, -1, 11, -1, 11, 11 },
				{ 11, 9, 6, 6, 6, 1, 6, 3, -1, 11, 11 },
				{ 11, 11, -1, -1, -1, 11, -1, 11, -1, 11, 11 },
				{ 13, 1, 2, 6, 6, 5, 2, 5, 6, 7, 11 },
				{ 10, 7, 9, 6, 6, 8, 13, 6, 6, 8, 11 },
				{ 13, 8, 13, 6, 6, 6, 6, 6, 6, 8, 15 }
		};
		
		startTilePosition = 523;
		startX = 0;
		startY = 0;

		goalTilePosition = 2822;
		goalX = 9;
		goalY = 5;
		
	}

}
