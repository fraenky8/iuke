package iuke.Labyrinths;

public class LabyrinthFrank extends AbstractLabyrinth implements
		LabyrinthInterface {

	public LabyrinthFrank() {

		// hardcoded

		labyrinth = new int[][] {
				{ 12, 10, 4, 14, 4, 14, 2, 6, 2, 2, 8 },
				{ 11, 15, 9, 6, 5, 6, 7, 10, 7, 9, 8 },
				{ 13, 2, 5, 8, 14, 7, 14, 1, 8, 13, 4 },
				{ 10, 7, -1, -1, -1, 12, -1, 11, -1, 14, 7 },
				{ 13, 2, 6, 6, 6, 5, 8, 11, 11, 10, 8 },
				{ 10, 3, -1, -1, -1, 12, -1, 15, -1, 13, 4 },
				{ 11, 15, 14, 6, 6, 5, 2, 4, 10, 6, 7 },
				{ 13, 2, 4, 10, 6, 8, 11, 13, 5, 6, 8 },
				{ 14, 7, 13, 5, 6, 6, 5, 6, 6, 6, 8 }, };

		startTilePosition = 1803;
		startX = 5;
		startY = 0;

		goalTilePosition = 1795;
		goalX = 5;
		goalY = 8;

	}

}
