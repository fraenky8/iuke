package iuke;

import iuke.Labyrinths.LabyrinthPeter;

/**
 *
 */
public class LabyrinthTester {

	static protected LabyrinthGame labyrinth;

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		labyrinth = new LabyrinthGame(new LabyrinthPeter());

		labyrinth.notify(523);
		// e.notify(2822);//Sprung an Ziel
		labyrinth.notify(779);// Wand -> Game over
		// e.notify(523);
		// e.notify(522);
		labyrinth.notify(778);
		labyrinth.notify(522);// Schritt zur�ck
		labyrinth.notify(523);// zur�ck an Start
		labyrinth.notify(2315);// Sprung an entfernte Stelle
		labyrinth.notify(523);// zur�ck an Start
		labyrinth.notify(267);// Feld au�erhalb des Spielfeldes
		labyrinth.notify(523);
		labyrinth.notify(778);// diagonal
		labyrinth.notify(523);// zur�ck an start
		labyrinth.notify(523);// erneutes event von gleicher stelle
		labyrinth.notify(522);
		labyrinth.notify(778);
		labyrinth.notify(777);// durch Wand
		labyrinth.notify(776);
		labyrinth.notify(775);
		labyrinth.notify(1031);
		labyrinth.notify(1287);
		labyrinth.notify(1543);
		labyrinth.notify(1799);
		labyrinth.notify(2055);
		labyrinth.notify(2311);
		labyrinth.notify(2310);
		labyrinth.notify(2309);
		labyrinth.notify(2565);
		labyrinth.notify(2822);// Ziel �ber diagonalen Schritt erreicht
		labyrinth.notify(2821);
		labyrinth.notify(2822);// Ziel erreicht
		labyrinth.notify(3078);

	}

}