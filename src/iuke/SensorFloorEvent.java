package iuke;

public class SensorFloorEvent {

	protected int tileID = 0;

	protected boolean activated = false;

	public SensorFloorEvent(int tileID, boolean activated) {
		this.tileID = tileID;
		this.activated = activated;
	}

	public Integer getTileId() {
		return this.tileID;
	}

	public Boolean isActivated() {
		return this.activated;
	}

}
