package iuke;

import iuke.Labyrinths.AbstractLabyrinth;

/**
 *
 */
public class LabyrinthGame {

	private static final int LEFT = 0;
	private static final int BOTTOM = 1;
	private static final int RIGHT = 2;
	private static final int TOP = 3;

	@SuppressWarnings("unused")
	private static final char PASS = '0'; // unused, daher suppressed
	private static final char WALL = '1';

	protected boolean isStarted = false;
	protected String pdfPageBinary;
	
	protected int[][] sensefloorTilesUsed = {
			{ 523, 779, 1035, 1291, 1547, 1803, 2059, 2315, 2571, 2827, 3083 },
			{ 522, 778, 1034, 1290, 1546, 1802, 2058, 2314, 2570, 2826, 3082 },
			{ 521, 777, 1033, 1289, 1545, 1801, 2057, 2313, 2569, 2825, 3081 },
			{ 520, 776, -1, -1, -1, 1800, -1, 2312, -1, 2824, 3080 },
			{ 519, 775, 1031, 1287, 1543, 1799, 2055, 2311, -1, 2823, 3079 },
			{ 518, 774, -1, -1, -1, 1798, -1, 2310, -1, 2822, 3078 },
			{ 517, 773, 1029, 1285, 1541, 1797, 2053, 2309, 2565, 2821, 3077 },
			{ 516, 772, 1028, 1284, 1540, 1796, 2052, 2308, 2564, 2820, 3076 },
			{ 515, 771, 1027, 1283, 1539, 1795, 2051, 2307, 2563, 2819, 3075 }
	};

	protected AbstractLabyrinth labyrinth;
	
	protected int currentXPosition;
	protected int currentYPosition;
	
	public LabyrinthGame(AbstractLabyrinth labyrinth) {
		this.labyrinth = labyrinth;
	}

	// TODO
	public void notify(SensorFloorEvent sfe) {
		System.out.println(sfe.getTileId());
		System.out.println(sfe.isActivated());
	}

	/**
	 * @param tileID
	 */
	public void notify(int tileID) {

		if (!isStarted(tileID))	{
			return;
		}

		// den Sensefloor nach der Tile durchsuchen
		int[] newXAndY = searchForTileInSensefloor(tileID);
		
		// Tile wurde nicht gefunden
		if (newXAndY == null)	{
			// DEBUG
			System.out.println("Außerhalb!");
			return;
		}
		
		// neuen Werte aus return-Array extrahieren
		int newX = newXAndY[0];
		int newY = newXAndY[1];
		
		// Richtung bestimmen
		int diffX = Math.abs(currentXPosition - newX);
		int diffY = Math.abs(currentYPosition - newY);
		
		if (!isUserStepValid(diffX, diffY))	{
			return;
		}

		// DEBUG
		System.out.println("Gültiger Schritt!");
		
		// Valid step
		setUserPosition(newX, newY);

	}

	/**
	 * 
	 * @param tileID
	 * @return
	 */
	protected boolean isStarted(int tileID)	{
		
		if (!isStarted) {
			
			// DEBUG
			System.out.println("Noch nicht gestartet!");
			
			if (tileID == labyrinth.getTileStartPosition()) {
				
				isStarted = true;
				currentXPosition = labyrinth.getStartPositionX();
				currentYPosition = labyrinth.getStartPositionY();
				
				// TODO fireEvent -> pdf Viewer
				System.out.println("PDF Seite: " + labyrinth.getPdfPageForXYPosition(currentXPosition, currentYPosition));
				System.out.println("Es geht los!!");
				
				return true;
			}
			
			return false;
		}
		
		return true;
	}
	
	/**
	 * 
	 * @param diffX
	 * @param diffY
	 * @return
	 */
	protected boolean isUserStepValid(int diffX, int diffY) {
		
		if (diffX == 0 && diffY == 0) { // kein Schritt gemacht
			
			// DEBUG - evtl auskommentieren, da sonst die Console bestimmt damit zugespamt wird
			System.out.println("keine Bewegung registriert");
			
			return false;
		}
		
		if (diffX == 1 && diffY == 1) { // Diagonalen Schritt gemacht

			// DEBUG
			System.out.println("Keine diagonalen Schritte!");
			
			return false;
		}
		
		if ((diffX != 0 || diffY != 1) && (diffX != 1 || diffY != 0))	{ // Außerhalb?

			// DEBUG
			System.out.println("Unerlaubter Schritt detektiert!");

			return false;
		}
		
		return true;
	}

	/**
	 * @param tileID
	 * @return array mit [0] = x und [1] = y Position
	 */
	protected int[] searchForTileInSensefloor(int tileID)	{
		for (int row = 0; row < sensefloorTilesUsed.length; row++) {
			for (int col = 0; col < sensefloorTilesUsed[row].length; col++) {
				if (sensefloorTilesUsed[row][col] == tileID) {
					
					// DEBUG
					System.out.println("Reihe: " + col + "; Spalte: " + row);
					
					int[] ret = {col, row}; 
					return ret;
				}
			}
		}
		
		return null;
	}
	
	/**
	 * 
	 * @param newXPosition
	 * @param newYPosition
	 */
	protected void setUserPosition(int newXPosition, int newYPosition) {

		// 4 bit:
		// links unten rechts oben
		// 1 = Wand, 0 = Durchgang

		/*
		 *       [3]
		 *       ___
		 * [0]  |   |  [2]
		 *      ´´´´´
		 *       [1]
		 */

		int pdfPage = labyrinth.getPdfPageForXYPosition(newXPosition, newYPosition);

		// pdf Seite in 4-bit Binary umwandeln
		pdfPageBinary = convertPdfPageTo4BitBinary(pdfPage);
				
		// DEBUG
		System.out.println("setUserPosition");
		System.out.println("x: " + newXPosition);
		System.out.println("y: " + newYPosition);

		// Richtung bestimmen, in der sich der Spieler bewegt hat
		int diffX = newXPosition - currentXPosition;
		int diffY = newYPosition - currentYPosition;

		// DEBUG
		System.out.println(pdfPageBinary);
		
		if (!isNewUserPositionValid(diffX, diffY))	{
			return;
		}
		
		// DEBUG
		System.out.println("alles OK");

		// Position updaten
		currentXPosition = newXPosition;
		currentYPosition = newYPosition;

		// DEBUG
		System.out.println("PDF Seite: " + pdfPage);
		
		
		// TODO fireEvent -> pdf Viewer
		

		// ggf Gewonnen schreien
		winingNotification(newXPosition, newYPosition);

	}

	/**
	 * 
	 * @param diffX
	 * @param diffY
	 * @return
	 */
	protected boolean isNewUserPositionValid(int diffX, int diffY)	{
		
		if (diffX == 1 && isWallAt(LEFT)) { // rechts gelaufen
			
			// DEBUG
			System.out.println("nach rechts gelaufen");
			
			losingNotification();
			return false;
		}
		
		if (diffX == -1 && isWallAt(RIGHT)) { // links gelaufen
			
			// DEBUG
			System.out.println("nach links gelaufen");
			
			losingNotification();
			return false;
		}
		
		if (diffY == 1 && isWallAt(TOP)) { // unten gelaufen
			
			// DEBUG
			System.out.println("nach unten gelaufen");
			
			losingNotification();
			return false;
		}
		
		if (diffY == -1 && isWallAt(BOTTOM)) { // oben gelaufen
			
			// DEBUG
			System.out.println("nach oben gelaufen");
			
			losingNotification();
			return false;
		}
		
		// drüfte NIE vorkommen,da abgefangen in notify()
		// daher entweder drin lassen oder rausschmeißen
		if (diffX != 1 && diffX != -1 && diffY != 1 && diffY != -1){
			System.out.println("Black Hole!!11");
		}
		
		return true;
	}
	
	/**
	 * 
	 * @param pdfPage
	 * @return
	 */
	protected String convertPdfPageTo4BitBinary(int pdfPage)	{
		// weil wir von 0 anfangen zu zählen und es keine 0-te Seite im PDF gibt, oder wie?
		pdfPage = pdfPage - 1;
		return fillWithZerosLeft(Integer.toBinaryString(pdfPage), 4);
	}
	
	/**
	 * 
	 * @param value
	 * @param len
	 * @return
	 */
	protected String fillWithZerosLeft(String value, int len) {
		while (value.length() < len) {
			value = "0" + value;
		}
		return value;
	}
	
	/**
	 * 
	 * @param position
	 * @return
	 */
	protected boolean isWallAt(int position)	{
		return pdfPageBinary.charAt(position) == WALL;
	}

	/**
	 * 
	 * @param newX
	 * @param newY
	 */
	protected void winingNotification(int newX, int newY) {

		if (newX == labyrinth.getGoalPositionX() && newY == labyrinth.getGoalPositionY()) {
			
			// DEBUG
			System.out.println("----> Gewonnen!! *BOOOMMM*");
			
			// TODO Sound etc
		}

	}

	/**
	 * 
	 */
	protected void losingNotification() {

		// DEBUG
		System.out.println("WAND!!");
		System.out.println("----> GAME OVER !!11!");

		isStarted = false;

		// TODO Sound, Explosion + PDF Seite
	}
}